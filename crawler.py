# auteur : Cédric Donner et la classe d'OCI

# Dans ce fichier, nous placerons les fonctions qui concernent le robot
# d'indexation

#les accents dans les commentaires produiront des erreurs lors de l'execution du fichier avec le bouton 'run'. Signe: Simon et Emerald ;-)

import urllib.request

from index import *

# index vide
index = []

debug = 0


# Fonction définie dans les vidéos 
# (corrigé détaillé sur http://www.donner-online.ch/oci/projet-1/corrige/partie-01/10-etape-01.html#solution-video-pas-a-pas-linux-ubuntu)
def get_page(url):
    try:
        fd = urllib.request.urlopen(url)
        html = fd.read()

        # il faut convertir les données en str car elles sont lues en bytes
        # depuis urllib
        # http://stackoverflow.com/a/542080
        return str(html)
    except:
        return ''

def get_next_target(page, start=0):
    '''
    
    Retourne une tuple (url, end) où url  est 
    l'URL (target) du premier lien rencontré dans le 
    code HTML ``page`` à partir de la position
    ``start`` et ``end`` indique la position de la
    fin de l'URL dans ``page``.
    
    S'il n'y a plus de lien après la position start, 
    la fonction retourne le tuple (None, -1).

    '''
    url = None
    pattern = '<a href="'
    start_tag = page.find(pattern, start)
    
    if start_tag == -1:
        return (None, -1)
    
    start_url = start_tag + len(pattern)
    end_url = page.find('"', start_url)
    
    url = page[start_url:end_url]
    
    return (url, end_url+1)
    
def print_all_links(html):
    '''
    
    Afficher tous les hyperliens contenus dans la chaine de caractères ``html``
    
    paramètres
    ==========
    
    * html ==> str : code HTML de la page dont on veut extraire les liens
    
    Valeur de retour : None
    
    '''
    
    start = 0
    
    if len(html) == 0:
        return None
        
    while start != -1:
        url, end = get_next_target(html, start)
        # on recommence la prochaine recherche depuis la fin du résultat courant
        start = end
        
        if url is not None:
            print(url)
    
    return None

def get_all_links(html):
    '''
    
    Retourne tous les hyperliens contenus dans la chaine de caractères ``html``
    sous forme d'une liste. 
    
    Si la page ne contient aucun lien, la fonction retourne la liste vide.
    
    paramètres
    ==========
    
    * html ==> str : code HTML de la page dont on veut extraire les liens
    
    Valeur de retour
    ================
    
    Liste contenant les liens (urls) et une liste vide si 
    la page ne contient aucun lien
    
    '''
    
    start = 0
    urls = []
    
    if len(html) == 0:
        return []
        
    while start != -1:
        url, end = get_next_target(html, start)
        # on recommence la prochaine recherche depuis la fin du résultat courant
        start = end
        
        if url is not None:
            urls.append(url)
    
    return urls

def crawl_web1(seed): 
    '''
    
    Navigue sur le Web en partant de l'URL ``seed`` et en suivant les liens
    qui se trouvent sur cette page.
    
    Valeur de retour
    ================
    
    Liste contenant les pages visitées. 
    
    '''

    tocrawl = [seed]
    crawled = [] 

    while tocrawl:
        
        page = tocrawl.pop() 
        
        if page not in crawled:
            if debug > 2: print("visite de la page : ", page)
            newpages = get_all_links(get_page(page))
            if debug > 2: print("nouvelles pages : ", newpages)
            
            # Problème : si tocrawl contient déjà une URL également présente
            # ``newpages``, tocrawl contiendra ensuite deux foix cette URL. Cela
            # va inévitablement nous mener visiter deux fois cette URL.
            
            # très inefficace, mais permet d'éviter de rajouter une URL de ``newpages``
            # déjà présente dans ``tocrawl``
            for url in newpages:
                if url not in tocrawl:
                    tocrawl = tocrawl + [url]

            crawled.append(page)
            
            if debug > 2: print("tocrawl : ")
            if debug > 2: print(tocrawl)
            
            if debug > 2: print("crawled : ")
            if debug > 2: print(crawled)

    return crawled
    
def get_keywords(html):
    mots = html.split(' ')
    return mots
    
def crawl_web2(seed): 
    '''
    
    Navigue sur le Web en partant de l'URL ``seed`` et en suivant les liens
    qui se trouvent sur cette page.
    
    Valeur de retour
    ================
    
    Liste contenant les pages visitées. 
    
    '''

    tocrawl = [seed]
    crawled = [] 

    while tocrawl:
        
        # url que l'on visite actuellement
        page = tocrawl.pop() 
        
        if page not in crawled:
            if debug > 2: print("visite de la page : ", page)
            html = get_page(page)
            newpages = get_all_links(html)
            if debug > 2: print("nouvelles pages : ", newpages)
            
            # indexation
            keywords = get_keywords(html)
            for kw in keywords:
                add_to_index(index, kw, page)
                
            # Problème : si tocrawl contient déjà une URL également présente
            # ``newpages``, tocrawl contiendra ensuite deux foix cette URL. Cela
            # va inévitablement nous mener visiter deux fois cette URL.
            
            # très inefficace, mais permet d'éviter de rajouter une URL de ``newpages``
            # déjà présente dans ``tocrawl``
            for url in newpages:
                if url not in tocrawl:
                    tocrawl = tocrawl + [url]

            crawled.append(page)
            
            if debug > 2: print("tocrawl : ")
            if debug > 2: print(tocrawl)
            
            if debug > 2: print("crawled : ")
            if debug > 2: print(crawled)

    return crawled    
    

##############################################################
### Tests unitaires des fonctions 
##############################################################
    
def test_crawl_web1():
    print('test de la fonction crawl_web ...')
    
    seeds = [
        'http://www.donner-online.ch/oci/projet-1/test-crawl-web/index.html',
        'http://www.donner-online.ch/oci/projet-1/test-crawl-web/loop.html'
        ]
    
    for seed in seeds:
        print("navigation a partir de l'url", seed)
        pages = crawl_web1(seed)
        print(pages)
    
def test_crawl_web2():
    crawl_web2('http://www.donner-online.ch/oci/projet-1/test-crawl-web/test/index.html')
    print(index)
    
def test_print_all_links():
    print_all_links('''bidule <a href="url1">lien</a> autre lien : <a href="url2">Lien 2</a>''')

def test_get_next_target():
    html = '''<html>\n    <body>\n        <a href="url1">Lien1</a>\n        un peu de texte\n        <a href="url2">Lien2</a>\n    </body>\n</html>\n'''
    print(get_next_target(html, start=0)) # ==> ('url1', 40)
    print(get_next_target(html, start=39)) # ==> ('url2', 97)
    print(get_next_target(html, start=97)) # ==> (None, -1)
    print(get_next_target(''))
    
def test_get_all_links():
    html = '''<html>\n    <body>\n        <a href="url1">Lien1</a>\n        un peu de texte\n        <a href="url2">Lien2</a>\n    </body>\n</html>\n'''    
    print('test de la fonction get_all_links ...')
    print(get_all_links(html) == ['url1', 'url2'])
    print(get_all_links('') == [])
    print(get_all_links('<html></html>') == [])
    

if __name__ == '__main__':
    #test_get_next_target()
    #test_print_all_links()
    #test_get_all_links()
    test_crawl_web2()