import random
import sys

from index import *

alphabet = 'abcdefghijklmnopqrstuvwxyz'
domainset = ['fr','ch','com','de','it','be','net','org']

def generate_rnd_string(min,max,charset):
    '''retourne une chaine de caracteres de taille comprise entre min et max, avec des caracteres de charset'''
    
    string = ''
    
    for i in range(random.randint(min,max)):
        string += random.choice(charset)
    
    return string
    


def generate_url():
    '''cree un url aleatoire de la forme http://<subdomaine>.<domaine>[/<chemin>][/<doc>.html]'''
    
    
    subdomain = generate_rnd_string(5,15,alphabet)
    domain = random.choice(domainset)
    chemin = generate_rnd_string(3,25,alphabet)
    doc = generate_rnd_string(3,10,alphabet)
    
    url = 'http://www.' + subdomain + '.' + domain
    
    slash = random.choice([True, False])
    
    if slash:
        url += '/'
    elif random.randint(0,1):
        url += '/' + chemin + '/' + doc + '.html'
        
    return url

def generate_random_index(size):

    # index vide
    index = []

    for i in range(size):

        # affichage de la progression tous les 50 mots-clés
        if i % 50 == 0:
            print("nb mot-clés", i)

        # génération du mot-clé
        kw = generate_rnd_string(5, 15, alphabet)
        nb_urls = random.randint(1, 20)

        # génération des urls
        for loop in range(nb_urls):
            url = generate_url()
            add_to_index(index, kw, url)

    return index
    
    
def generate_random_index_fast(size, minmax_url=None, minmax_kwlen=None, show_prog=False):
    '''

    Génère un index comptant ``size`` entrées. La longueur des mots-clés
    est un nombre aléatoire entre ``minmax_kwlen[0]`` et ``minmax_kwlen[1]``.

    Le nombre d'urls par mot-clé est compris entre ``minmax_url[0]`` et
    ``minmax_url[1]``.

    '''

    minmax_kwlen = minmax_kwlen or (5, 50)
    minmax_url = minmax_url or (1, 20)

    # index vide initialisé avec la bonne taille dès le départ
    index = [0] * size
    
    for no_kw in range(size):
       
        kw = generate_rnd_string(minmax_kwlen[0], minmax_kwlen[1], alphabet)
        nb_urls = random.randint(minmax_url[0], minmax_url[1])
        
        # initialisation de l'entrée de l'index avec la bonne taille pour accueillir
        # le nombre d'urls demandé
        index[no_kw] = ['', [0] * nb_urls]
        index[no_kw][0] = kw

        # affichage de la progression de la construction
        if show_prog and no_kw % int(size/100) == 0:
            print("nb mot-clés", no_kw, 'nb urls', nb_urls)
        
        for no_url in range(nb_urls):
            url = generate_url()
            index[no_kw][1][no_url] = url
    
    return index

    
def test_generate_url():
    for i in range(10):
        print(generate_url())


if __name__ == '__main__':
##    test_generate_url()
##    index = generate_random_index(size=10**4)
    index = generate_random_index_fast(size=500, minmax_url=(1,30000), show_prog=True)

    
    
    
