# auteurs : OCI 2014 
# fichier : index_dict.py

'''

Ce fichier définit la structure de données et les opérations pour gérer l'index
du moteur de recherche en utilisant un dictionnaire plutôt qu'une liste pour
améliorer les performances.

'''

index = {}

def lookup(index,kw):
    '''
    
    implémentation de lookup en utilisant un dictionnaire. Retourne les urls
    associées au mot-clé ``kw``
    
    '''
    try: 
        return index[kw]
    except: 
        return []
        
# autre définition possible pour la fonction lookup() en testant la présence
# du mot-clé avec l'opérateur ``in``

def lookup_alt(index, kw):
    if kw in index:
        return index[kw]
    
    return []
      

def add_to_index(index,kw,url):
    """fonction qui verifie la presence du kw puis de l url et fait les ajouts a l index
        cette fonction ne retourne rien """
    if kw in index:
        if url not in index[kw]:
            index[kw] += [url]
    
    else:
        index[kw]=[url]

    return None
    