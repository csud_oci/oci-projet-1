##############################################################
### Structure de données à utiliser pour l'index
##############################################################

# ancienne structure de données pour laquelle le code fonctionne. 
index2 = [['Python', 'http://www.python.org', 'http://www.donner-online.ch/oci/'],
          ['Collège', 'http://www.collegedusud.ch']]

# structure de données choisir pour faire la suite du projet. Pour le moment,
# les fonctions dans ce code ne fonctionnent pas avec cette implémentation. Il
# faut modifier le moins possible de fonctions pour que le code tourne avec
# cette implémentation.
index4 = [['Python', ['http://www.python.org', 'http://www.donner-onlinech/oci']],
          ['Informatique', ['http://www.soi.ch', 'http://concours.castor-informatique.ch/']]
         ]

# indiquer l'implémentation utilisée
index = index4



##############################################################
### Fonctions auxiliaires
##############################################################


def get_kw(entry):
    return entry[0]

def get_urls(entry):
    return entry[1]

def add_entry_to_index(index, keyword, url):
    index.append([keyword, [url]])

def add_url_to_entry(entry, url):
    entry[1].append(url)

def get_kw_position(index, keyword):
    '''

    Retourne le numéro de l'entrée contenant le mot-clé spécifié par
    ``keyword`` dans l'index.

    Si aucune entrée de l'index ne correspond au mot-clé spécifié, la fonction
    retourne -1

    '''
    position = -1

    i = 0
    while i < len(index):
        if get_kw(index[i]) == keyword:
            position = i
            break            
        i += 1

    return position



##############################################################
### Fonctions principales
##############################################################


def lookup(index, keyword):
    '''
    
    La fonction lookup permet de rechercher dans l'index toutes les urls qui
    sont associées au mot clé indiqué par ``keyword``. 

    Valeur de retour : list d'urls

    Si le mot ``keyword``
    ne figure pas encore dans l'index, la fonction retourne une liste vide.

    '''

    urls = []

    for entry in index:
        if get_kw(entry) == keyword:
            urls = get_urls(entry)
            break

    return urls

    # retourner la liste d'urls
    return urls


def add_to_index(index, keyword, url):

    # il faut s'assurer que le mot-clé n'existe pas déjà dans l'index
    position = get_kw_position(index, keyword) 
    if position == -1:
        # le mot-clé ``keyword``ne se trouve pas encore dans l'index. Il faut
        # donc rajouter une entrée dans l'index
        add_entry_to_index(index, keyword, url)

    else:
        # le mot-clé ``keyword``se trouve déjà dans l'index. Il faut donc
        # rajouter l'url à cette entrée
        add_url_to_entry(index[position], url)


##############################################################
### Tests unitaires des fonctions 
##############################################################



def test_lookup():
    print(lookup(index, 'Python'))
    print(lookup(index, 'College'))
    print(lookup(index, 'MotCleInexistant'))

def test_add_to_index():
    index = [['Python', ['http://www.python.org', 'http://www.donner-online.ch/oci/']],
             ['Collège', ['http://www.collegedusud.ch']]]

    add_to_index(index, 'new-kw', 'http://new.com')
    print('test ajout nouveau mot-cle dans index ...')
    print('test de add_to_index ... ==> ', index[2][0] == 'new-kw')
    print('test de add_to_index ... ==> ', index[2][1][0] == 'http://new.com')

    add_to_index(index, 'new-kw', 'http://other.com')
    print('test ajout url a mot-cle existant ... ')
    print('test de add_to_index ... ==> ', index[2][1][1] == 'http://other.com')

def test_get_kw_position():
    index = [['Python', ['http://www.python.org', 'http://www.donner-online.ch/oci/']],
             ['Collège', ['http://www.collegedusud.ch']]]

    print(get_kw_position(index, 'Python'))
    print(get_kw_position(index, 'College'))
    print(get_kw_position(index, 'MotCleInexistant'))

if __name__ == '__main__':
    test_lookup()
    test_get_kw_position()
    test_add_to_index()